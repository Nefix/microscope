package log

// TODO: Implement OTEL logging when it's supported by the Go SDK

// Copied from https://github.com/ttys3/slogx/blob/main/tracing_handler.go

import (
	"context"
	"log/slog"

	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

const traceIDKey = "trace_id"

var _ slog.Handler = &TracingHandler{}

func NewTracingHandler(h slog.Handler) *TracingHandler {
	// Avoid chains of TracingHandlers.
	if lh, ok := h.(*TracingHandler); ok {
		h = lh.Handler()
	}

	return &TracingHandler{
		handler: h,
	}
}

type TracingHandler struct {
	handler slog.Handler
}

func (h *TracingHandler) Enabled(ctx context.Context, lvl slog.Level) bool {
	return h.handler.Enabled(ctx, lvl)
}

func (h *TracingHandler) Handle(ctx context.Context, rec slog.Record) error {
	if span := trace.SpanFromContext(ctx); span.IsRecording() {
		// Add the trace id to the log attributes
		if sCtx := span.SpanContext(); sCtx.HasTraceID() {
			rec.AddAttrs(slog.String(traceIDKey, sCtx.TraceID().String()))
		}

		// If the log is an error log, set the span status and code
		if rec.Level >= slog.LevelError {
			span.SetStatus(codes.Error, rec.Message)
		}
	}

	return h.handler.Handle(ctx, rec)
}

func (h *TracingHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return NewTracingHandler(h.Handler().WithAttrs(attrs))
}

func (h *TracingHandler) WithGroup(name string) slog.Handler {
	return NewTracingHandler(h.Handler().WithGroup(name))
}

func (h *TracingHandler) Handler() slog.Handler {
	return h.handler
}
