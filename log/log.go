package log

import (
	"log/slog"
	"os"
)

func New(svc string, lvl slog.Leveler) *slog.Logger {
	log := slog.New(NewTracingHandler(
		slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
			Level: lvl,
		}),
	))
	log = log.With(slog.String("service", svc))
	slog.SetDefault(log)

	return log
}
