.PHONY: all
all: tidy lint test

.PHONY: tidy
tidy:
	go mod tidy

.PHONY: lint
lint: lint-yaml lint-go

.PHONY: lint-yaml
lint-yaml:
	yamllint .

.PHONY: lint-go
lint-go:
	golangci-lint run

.PHONY: test
test: test-go

.PHONY: test-go
test-go:
	go test -cover -race ./...
