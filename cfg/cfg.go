package cfg

import (
	"errors"
	"log/slog"
	"os"

	"github.com/spf13/viper"
)

func Init(log *slog.Logger, svc string, base Base) {
	cfg := viper.New()
	cfg.SetConfigType("json")
	cfg.SetConfigName(svc)
	cfg.AddConfigPath("/config")

	// TODO: Live reload
	// TODO: Environment variables

	if err := cfg.ReadInConfig(); err != nil {
		if !errors.As(err, &viper.ConfigFileNotFoundError{}) {
			log.Error("read configuration", slog.String("err", err.Error()))
			os.Exit(1)
		}
	}

	base.SetDefaults()

	if err := cfg.Unmarshal(base); err != nil {
		log.Error("unmarshal the initial configuration", slog.String("err", err.Error()))
		os.Exit(1)
	}
}

// Base represents the configuration that all the services need to have.
type Base interface {
	SetDefaults()

	LogLevel() slog.Leveler
}

var _ Base = &BaseConfig{}

type BaseConfig struct {
	Log struct {
		Level slog.Leveler
	}
}

func (b *BaseConfig) SetDefaults() {
	b.Log.Level = slog.LevelInfo
}

func (b BaseConfig) LogLevel() slog.Leveler {
	return b.Log.Level
}
