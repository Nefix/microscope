package cfg

import "fmt"

// Transport represents a configuration any transport available.
type Transport interface {
	transport()
	SetDefaults()
}

// GRPC represents the configuration required for a gRPC server.
type GRPC interface {
	transport()
	SetDefaults()

	// Addr will have the format "host:port"
	Addr() string
}

var _ GRPC = &GRPCConfig{}

type GRPCConfig struct {
	GRPC struct {
		Host string
		Port int
	}
}

func (GRPCConfig) transport() {}

func (g *GRPCConfig) SetDefaults() {
	g.GRPC.Host = "0.0.0.0"
	g.GRPC.Port = 1312
}

func (g GRPCConfig) Addr() string {
	return fmt.Sprintf("%s:%d", g.GRPC.Host, g.GRPC.Port)
}
