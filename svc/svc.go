package svc

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"sync"

	"gitlab.com/Nefix/microscope/cfg"
	"gitlab.com/Nefix/microscope/log"
	"gitlab.com/Nefix/microscope/transport"
)

type ServiceConfig interface {
	cfg.Base
	cfg.Transport
}

type Service[Config ServiceConfig] struct {
	Name       string
	Logger     *slog.Logger
	Transports []transport.Transport
	Config     Config
}

func NewService[Config ServiceConfig](_ context.Context, name string, config Config) *Service[Config] {
	// Here we create a temporary logger, because we don't know the actual level
	tmpLog := log.New(name, slog.LevelInfo)

	cfg.Init(tmpLog, name, config)

	return &Service[Config]{
		Name:       name,
		Logger:     log.New(name, config.LogLevel()),
		Transports: []transport.Transport{},
		Config:     config,
	}
}

func (s *Service[Config]) Run(ctx context.Context) {
	ctx, cancel := context.WithCancel(ctx)

	var wg sync.WaitGroup

	for _, t := range s.Transports {
		wg.Add(1)

		go func() {
			t.Serve(ctx, cancel)
			wg.Done()
		}()
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	<-stop
	fmt.Fprintln(os.Stdout, "")
	s.Logger.Info("stopping service")

	cancel()
	wg.Wait()
}

func (s *Service[Config]) RegisterTransport(t transport.Transport) {
	t.Register(s.Logger, s.Config)

	s.Transports = append(s.Transports, t)
}
