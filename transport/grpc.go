package transport

import (
	"context"
	"log/slog"
	"net"
	"os"

	"gitlab.com/Nefix/microscope/cfg"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type GRPC struct {
	logger *slog.Logger
	addr   string

	desc    *grpc.ServiceDesc
	handler any

	server *grpc.Server
}

func NewGRPC(desc *grpc.ServiceDesc, handler any) *GRPC {
	return &GRPC{
		desc:    desc,
		handler: handler,
	}
}

func (g *GRPC) Register(logger *slog.Logger, traCfg cfg.Transport) {
	// TODO: add type safety here
	config, ok := traCfg.(cfg.GRPC)
	if !ok {
		logger.Error("invalid configuration type, was expecting cfg.GRPC")
		os.Exit(1)
	}

	g.logger = logger
	g.addr = config.Addr()

	srv := grpc.NewServer(
		grpc.StatsHandler(otelgrpc.NewServerHandler()),
	)

	srv.RegisterService(g.desc, g.handler)
	reflection.Register(srv)

	// TODO: healthcheck

	g.server = srv
}

func (g *GRPC) Serve(ctx context.Context, cancel context.CancelFunc) {
	log := g.logger.With(slog.String("addr", g.addr))

	lis, err := net.Listen("tcp", g.addr)
	if err != nil {
		log.Error("listen gRPC TCP address", slog.String("err", err.Error()))
		cancel()

		return
	}

	go func() {
		if err := g.server.Serve(lis); err != nil {
			log.Error("serve gRPC", slog.String("err", err.Error()))
			cancel()
		}
	}()

	defer g.server.GracefulStop()

	log.Info("serving through transport gRPC")

	<-ctx.Done()
}
