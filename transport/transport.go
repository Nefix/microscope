package transport

import (
	"context"
	"log/slog"

	"gitlab.com/Nefix/microscope/cfg"
)

type Transport interface {
	Register(log *slog.Logger, cfg cfg.Transport)
	Serve(ctx context.Context, cancel context.CancelFunc)
}
